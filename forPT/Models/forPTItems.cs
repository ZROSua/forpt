﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace forPT.Models
{
    public class forPTItems
    {
        public forPTItems(string name)
        {
            Name = name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
