﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace forPT.Models
{
    public class forPTContext : DbContext
    {
        public DbSet<forPTItems> forPTItems { get; set; }
        public forPTContext(DbContextOptions<forPTContext> options)
            : base(options)
        {
        }
    }
}
