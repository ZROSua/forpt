﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using forPT.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace forPT.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly forPTContext _context;

        public ValuesController(forPTContext context)
        {
            _context = context;
        }

        
        [HttpGet("forPTItems")]
        public IActionResult Get()
        {
            return Json(_context.forPTItems.AsEnumerable());
        }

        
        [HttpGet("forPT/{id}")]
        public ActionResult<string> Get(int id)
        {
            return Json(_context.forPTItems.SingleOrDefault(x=>x.Id==id));
        }

        
        [HttpPost("forPTItems")]
        public IActionResult Post([FromBody] CreateforPT command)
        {
            var forPTItems = new forPTItems(command.Name);
            _context.forPTItems.Add(forPTItems);
            _context.SaveChanges();
            return StatusCode(201);
        }

        
        [HttpPut("forPTItems/{id}")]
        public IActionResult Update(int id, [FromBody] CreateforPT createforPT)
        {
            var forPT = _context.forPTItems.SingleOrDefault(x => x.Id == id);
            forPT.Name = createforPT.Name;
            _context.Update(forPT);
            _context.SaveChanges();
            return StatusCode(204);
        }

        
        [HttpDelete("forPTItems/{id}")]
        public IActionResult Delete(int id)
        {
            var forPT = _context.forPTItems.SingleOrDefault(x => x.Id == id);
            _context.forPTItems.Remove(forPT);
            _context.SaveChanges();
            return StatusCode(202);
        }
    }
}
